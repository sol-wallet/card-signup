import { createContext } from 'react';

const State = createContext(null);

State.displayName = 'AppState';

export default State;
