import React from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import 'twin.macro';

import { URL } from 'const';
import { ArrowLeftIcon, CloseIcon } from 'assets/icons';

import { StyledButton } from './styles';

const pathToTitle = {
  [URL.DELIVERY_ADDRESS]: 'Where to deliver?',
  [URL.PICKUP]: 'Pickup point',
  [URL.PAYMENT]: 'Payment',
  [URL.STATUS]: 'Status',
  [URL.CARD_ACTIVATION]: 'Card activation',
};

export default function AppHeader() {
  const history = useHistory();
  const location = useLocation();

  const title = pathToTitle[location.pathname];
  const isLeftButtonDisplayed = ![URL.HOME, URL.STATUS].includes(location.pathname);
  const isRightButtonDisplayed = [URL.HOME, URL.STATUS].includes(location.pathname);

  return (
    <div tw="relative min-h-56 flex items-center justify-center mb-14 z-10">
      {isLeftButtonDisplayed && (
        <StyledButton onClick={history.goBack} tw="left-0">
          <ArrowLeftIcon />
        </StyledButton>
      )}

      {title && <h2 tw="text-20 leading-28 font-medium">{title}</h2>}

      {isRightButtonDisplayed && (
        <StyledButton href="solwallet://link?action=close_screen" tw="right-0 bg-black-200 text-white rounded-full">
          <CloseIcon />
        </StyledButton>
      )}
    </div>
  );
}
