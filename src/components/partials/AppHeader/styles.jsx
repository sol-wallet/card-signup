import tw, { styled } from 'twin.macro';

export const StyledButton = styled.a`
  ${tw`absolute top-0 bottom-0 m-auto flex items-center justify-center`}
  width: 3rem;
  height: 3rem;
`;
