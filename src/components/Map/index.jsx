import React, { useContext, useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import 'twin.macro';

import State from 'context';

import { GoogleMap, Marker, DirectionsService, DirectionsRenderer, useLoadScript } from '@react-google-maps/api';
import { isSameLocation } from 'utils';

import { CloseIcon, DashIcon, NavigationIcon } from 'assets/icons';
import markerInactiveIcon from 'assets/icons/marker-inactive.svg';
import markerActiveIcon from 'assets/icons/marker-active.svg';

import MapAgentInfo from './AgentInfo';

const DEFAULT_USER_LOCATION = { lat: -26.2707593, lng: 28.1122679, heading: 0 };

function Map({ agents, className, selectedAgentIndex, changeSelected, ...props }) {
  const selectedAgent = selectedAgentIndex !== null ? agents[selectedAgentIndex] : null;
  const { map, setMap } = useContext(State);

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_MAPS_TOKEN,
  });
  const [userLocation, setUserLocation] = useState(null);
  const [destination, setDestination] = useState(null);
  const [directions, setDirections] = useState(null);
  const [showDirections, setShowDirections] = useState(false);

  function directionsCallback(resp) {
    if (resp !== null) {
      if (resp.status === 'OK') {
        setDirections(resp);
      }
    }
  }

  function constructRoute() {
    if (isSameLocation(selectedAgent, destination)) {
      setShowDirections(!showDirections);
    } else {
      setShowDirections(true);
      setDestination({ lat: selectedAgent.lat, lng: selectedAgent.lng });
    }
  }

  const mapDirections = useMemo(
    () => (
      <>
        {destination && (
          <DirectionsService
            options={{
              destination,
              origin: userLocation,
              travelMode: 'DRIVING',
            }}
            callback={directionsCallback}
          />
        )}

        {showDirections && directions && <DirectionsRenderer options={{ directions, suppressMarkers: true }} />}
      </>
    ),
    [destination, directions, showDirections, userLocation],
  );

  useEffect(() => {
    const watchID = navigator.geolocation.watchPosition(
      ({ coords }) => {
        setUserLocation({
          lat: coords.latitude,
          lng: coords.longitude,
          heading: coords.heading || 0,
        });
      },
      () => {
        setUserLocation(DEFAULT_USER_LOCATION);
      },
      {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000,
      },
    );

    return () => {
      navigator.geolocation.clearWatch(watchID);
    };
  }, []);

  let mapContent;

  if (!navigator.geolocation) {
    mapContent = <p>Can&apos;t get user location</p>;
  } else if (loadError) {
    mapContent = <p>Sorry, map cannot be loaded right now</p>;
  } else if (!(userLocation && isLoaded)) {
    mapContent = null;
  } else {
    mapContent = (
      <GoogleMap
        {...props}
        mapContainerStyle={{ width: '100%', height: '100%' }}
        zoom={10}
        options={{
          disableDefaultUI: true,
          gestureHandling: 'greedy',
        }}
        onClick={() => changeSelected(null)}
        onLoad={m => {
          m.setCenter(userLocation);
          setMap(m);
        }}
      >
        {mapDirections}
        <Marker
          position={userLocation}
          icon={{
            path: window.google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
            rotation: userLocation.heading,
            scale: 4,
            strokeColor: '#021318',
            strokeWeight: 3,
          }}
          onClick={() => {
            map.panTo(userLocation);
          }}
        />
        {agents.map(({ lat, lng, phone }, index) => (
          <Marker
            position={{ lat, lng }}
            icon={selectedAgentIndex === index ? markerActiveIcon : markerInactiveIcon}
            onClick={() => {
              const { lat: lt, lng: ln } = agents[index];
              map.panTo({ lat: lt, lng: ln });
              changeSelected(index);
            }}
            key={phone}
          />
        ))}
        {selectedAgentIndex !== null && (
          <MapAgentInfo
            agentData={selectedAgent}
            destination={destination}
            showDirections={showDirections}
            constructRoute={constructRoute}
          />
        )}
        <div tw="absolute z-20 top-0 bottom-0 text-black-100 flex flex-col justify-center right-0 mr-16 pointer-events-none">
          <button
            type="button"
            tw="flex justify-center items-center w-40 h-40 bg-yellow-100 rounded-full mb-18 mt-auto pointer-events-auto"
            onClick={() => map.setZoom(map.getZoom() + 1)}
          >
            <CloseIcon tw="transform rotate-45" />
          </button>
          <button
            type="button"
            tw="flex justify-center items-center w-40 h-40 bg-yellow-100 rounded-full mb-18 pointer-events-auto"
            onClick={() => map.setZoom(map.getZoom() - 1)}
          >
            <DashIcon tw="w-24 h-24" />
          </button>
          <button
            type="button"
            tw="flex justify-center items-center w-40 h-40 bg-yellow-100 rounded-full mb-auto pointer-events-auto"
            onClick={() => map.panTo(userLocation)}
          >
            <NavigationIcon tw="w-24 h-24" />
          </button>
        </div>
      </GoogleMap>
    );
  }

  return (
    <div
      tw="w-screen flex justify-center items-center -mx-16 -mt-14"
      style={{ height: 'calc(100vh - 5.6rem' }}
      className={className}
    >
      {mapContent}
    </div>
  );
}

Map.propTypes = {
  agents: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  className: PropTypes.string,
  selectedAgentIndex: PropTypes.number,
  changeSelected: PropTypes.func.isRequired,
};

Map.defaultProps = {
  className: '',
  selectedAgentIndex: null,
};

export default React.memo(Map);
