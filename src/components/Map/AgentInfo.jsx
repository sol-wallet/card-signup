import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import tw from 'twin.macro';

import State from 'context';
import { URL } from 'const';

import { Button } from 'components/general';
import { isWorkingHours, isSameLocation } from 'utils';

import { CloseIcon, RouteIcon } from 'assets/icons';

export default function MapAgentInfo({
  agentData: { first_name, last_name, phone, address, distance, lat, lng, work_time_begin, work_time_end },
  destination,
  showDirections,
  constructRoute,
}) {
  const { setAgentInfo } = useContext(State);
  const history = useHistory();

  function handleAgentSelection() {
    setAgentInfo({ first_name, last_name, address, phone });
    history.push(URL.PAYMENT);
  }

  const isWorking = isWorkingHours(work_time_begin, work_time_end);
  const isRouteConstructed = isSameLocation({ lat, lng }, destination) && showDirections;
  const Icon = isRouteConstructed ? CloseIcon : RouteIcon;

  return (
    <div tw="absolute z-30 left-0 right-0 bottom-0 flex flex-col bg-black-200 text-white rounded-t-18 px-16 pt-14 pb-34">
      <div tw="flex items-center justify-between mb-6">
        <p tw="flex items-center">
          <span tw="text-18 leading-20 tracking-tight mr-8">
            {first_name} {last_name}
          </span>
        </p>
        <p tw="text-gray-500 font-medium"> ({distance} km)</p>
      </div>
      <p css={['mb-12', isWorking && tw`text-green-300`]}>
        {isWorking ? `${work_time_begin} - ${work_time_end}` : `Working hours: ${work_time_begin} - ${work_time_end}`}
      </p>
      <p tw="text-gray-500 flex justify-between mb-16">
        {address}
        <button
          type="button"
          tw="flex-shrink-0 flex justify-center items-center w-40 h-40 bg-gray-999 text-white rounded-full"
          onClick={constructRoute}
        >
          <Icon css={['w-24 h-24', isRouteConstructed && 'transform rotate-45']} />
        </button>
      </p>
      <Button color="green" onClick={handleAgentSelection}>
        I&apos;ll pick it up here
      </Button>
    </div>
  );
}

MapAgentInfo.propTypes = {
  agentData: PropTypes.shape().isRequired,
  destination: PropTypes.shape(),
  showDirections: PropTypes.bool.isRequired,
  constructRoute: PropTypes.func.isRequired,
};

MapAgentInfo.defaultProps = {
  destination: null,
};
