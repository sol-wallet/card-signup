import React from 'react';
import PropTypes from 'prop-types';

import 'twin.macro';
import Input from './Input/Input';
import Select from './Select/Select';

export default function FormItem({ el, id, label, ...inputProps }) {
  let Component;

  switch (el) {
    case 'select':
      Component = Select;
      break;
    case 'input':
    default:
      Component = Input;
      break;
  }

  return (
    <div tw="flex flex-col">
      <label htmlFor={id} tw="text-14 text-gray-500 leading-18 tracking-02 mb-8">
        {label}
      </label>
      <Component id={id} {...inputProps} />
    </div>
  );
}

FormItem.propTypes = {
  el: PropTypes.oneOf(['input', 'select']),
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

FormItem.defaultProps = {
  el: 'input',
};
