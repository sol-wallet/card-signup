import React from 'react';
import PropTypes from 'prop-types';
import 'twin.macro';
import InputMask from 'react-input-mask';
import { inputStyles } from './styles';

export default function Input({ mask, ...props }) {
  let Element;
  if (mask) {
    Element = InputMask;
  } else {
    Element = 'input';
  }

  return <Element mask={mask} css={inputStyles} {...props} />;
}

Input.propTypes = {
  mask: PropTypes.string,
};

Input.defaultProps = {
  mask: '',
};
