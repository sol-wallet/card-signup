import tw, { theme, css } from 'twin.macro';

export const inputStyles = css`
  ${tw`bg-black-200 leading-20 tracking-01 p-12 rounded-8 focus:border-yellow-100`}
  border: 0.5px solid ${theme`colors.black.200`};
`;
