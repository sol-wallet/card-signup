import React from 'react';
import ReactSelect from 'react-select';

import { selectStyles } from './styles';

export default function Select(props) {
  return <ReactSelect styles={selectStyles} {...props} />;
}
