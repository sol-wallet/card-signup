import tw, { theme, css } from 'twin.macro';

export const selectStyles = {
  singleValue: styles => ({ ...styles, color: theme`colors.white` }),
  menu: styles => ({ ...styles, backgroundColor: theme`colors.black.200` }),
  option: (styles, { isSelected, isFocused }) => {
    const optionStyles = {
      backgroundColor: '',
      color: '',
      ':active': { backgroundColor: theme`colors.yellow.200` },
    };

    if (isSelected) {
      optionStyles.color = theme`colors.black.100`;
      optionStyles.backgroundColor = theme`colors.yellow.100`;
    } else if (isFocused) {
      optionStyles.backgroundColor = theme`colors.yellow.200`;
      optionStyles.color = theme`colors.black.100`;
    } else {
      optionStyles.backgroundColor = 'transparent';
    }

    return {
      ...styles,
      ...optionStyles,
    };
  },
  indicatorSeparator: () => ({}),
  control: (_styles, state) => css`
    ${tw`flex bg-black-200 leading-20 tracking-01 py-3 rounded-8`}
    border: 0.5px solid ${theme`colors.black.200`};
    ${state.isFocused && tw`border-yellow-100`}
  `,
};
