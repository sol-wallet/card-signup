import React from 'react';
import PropTypes from 'prop-types';
import tw from 'twin.macro';

import { Link } from 'react-router-dom';

export default function Button({ color, to, href, disabled, children, ...props }) {
  let Element;
  let colorStyles;
  const additionalProps = {};

  switch (color) {
    case 'green':
      colorStyles = tw`bg-green-200 text-white`;
      break;
    case 'gray':
      colorStyles = tw`bg-black-500 text-white`;
      break;
    case 'yellow':
    default:
      colorStyles = tw`bg-yellow-100 text-black-100`;
      break;
  }

  if (to && !disabled) {
    Element = Link;
    additionalProps.to = to;
  } else if (href && !disabled) {
    Element = 'a';
    additionalProps.href = href;
  } else {
    Element = 'button';
    additionalProps.type = 'button';
  }

  return (
    <Element
      disabled={disabled}
      tw="inline-block text-center leading-20 font-medium p-12 rounded-8"
      css={[disabled && tw`bg-gray-700 text-white`, !disabled && colorStyles]}
      {...additionalProps}
      {...props}
    >
      {children}
    </Element>
  );
}

Button.propTypes = {
  color: PropTypes.string,
  to: PropTypes.string,
  href: PropTypes.string,
  disabled: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

Button.defaultProps = {
  to: null,
  href: null,
  color: 'yellow',
  disabled: false,
};
