import tw, { styled, css } from 'twin.macro';

export const StyledStep = styled.div(({ isDone, isCurrent }) => [
  css`
    ${tw`relative flex justify-center items-center text-left text-12 leading-18`}

    &:not(:first-of-type)::before {
      content: '';
      display: block;
      width: 3.4rem;
      height: 1px;
      ${isDone && tw`bg-green-400`};
      ${isCurrent && tw`bg-yellow-200`};
      ${!(isDone || isCurrent) && tw`bg-gray-400`};
    }
  `,
]);

export const StyledStepIcon = styled.div(
  ({ isDone, isCurrent }) => css`
    ${tw`bg-black-400 flex justify-center items-center rounded-full border w-28 h-28`}
    ${isDone && tw`text-green-400 border-green-400`}
    ${isCurrent && tw`text-yellow-200 border-yellow-200`}
    ${!(isDone || isCurrent) && tw`text-gray-400 border-gray-400`}
  `,
);

export const StyledStepText = styled.span`
  ${tw`absolute`}

  bottom: -2.4rem;
  right: 1.4rem;
  transform: translateX(50%);
  width: max-content;
`;
