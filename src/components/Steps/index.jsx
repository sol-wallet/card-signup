import React from 'react';
import PropTypes from 'prop-types';
import 'twin.macro';

import { CheckIcon, CircleIcon } from 'assets/icons';
import { ORDER_STATUS } from 'const';

import { StyledStep, StyledStepText, StyledStepIcon } from './styles';

export default function Steps({ orderStatus }) {
  const steps = [
    { id: 'not_paid', name: 'Order' },
    { id: 'paid', name: 'Paid' },
    { id: 'in_delivery', name: 'Preparing' },
    { id: 'delivered', name: orderStatus === ORDER_STATUS.ACTIVATED ? 'Delivered' : 'In delivery' },
  ];

  for (let i = 0; i < steps.length; i += 1) {
    steps[i].isDone = true;

    if (steps[i].id === orderStatus) {
      steps[i + 1].isCurrent = true;
      break;
    }
  }

  return (
    <div tw="flex justify-center my-24 pb-24">
      {steps.map(({ id, name, isDone, isCurrent }) => (
        <StyledStep isDone={isDone} isCurrent={isCurrent} key={id}>
          <StyledStepIcon isDone={isDone} isCurrent={isCurrent}>
            {isDone && <CheckIcon />}
            {isCurrent && <CircleIcon />}
          </StyledStepIcon>
          <StyledStepText>{name}</StyledStepText>
        </StyledStep>
      ))}
    </div>
  );
}

Steps.propTypes = {
  orderStatus: PropTypes.string.isRequired,
};
