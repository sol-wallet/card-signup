import { lazy } from 'react';

import { URL } from 'const';

const Home = lazy(() => import('pages/Home'));
const DeliveryMethod = lazy(() => import('pages/DeliveryMethod'));
const DeliveryAddress = lazy(() => import('pages/DeliveryAddress'));
const Pickup = lazy(() => import('pages/Pickup'));
const Payment = lazy(() => import('pages/Payment'));
const Status = lazy(() => import('pages/Status'));
const CardActivation = lazy(() => import('pages/CardActivation'));

export default [
  { id: 'DELIVERY_METHOD', path: URL.DELIVERY_METHOD, Component: DeliveryMethod },
  { id: 'DELIVERY_ADDRESS', path: URL.DELIVERY_ADDRESS, Component: DeliveryAddress },
  { id: 'PICKUP', path: URL.PICKUP, Component: Pickup },
  { id: 'PAYMENT', path: URL.PAYMENT, Component: Payment },
  { id: 'STATUS', path: URL.STATUS, Component: Status },
  { id: 'CARD_ACTIVATION', path: URL.CARD_ACTIVATION, Component: CardActivation },
  { id: 'HOME', path: URL.HOME, Component: Home },
];
