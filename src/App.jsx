import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import State from 'context';

import { getOrderStatus } from 'api';

import { GlobalStyles } from 'twin.macro';
import { AppHeader } from 'components/partials';

import { URL } from 'const';

import routes from 'routes';

import './baseStyles.css';
import { StyledApp } from './styles';
import { CloseIcon } from './assets/icons';

export default function App() {
  const [apiError, setError] = useState(null);
  const [map, setMap] = useState(null);
  const [agentInfo, setAgentInfo] = useState(null);
  const [activated, setActivated] = useState(false);

  const [deliveryType, setDeliveryType] = useState(null);
  const [deliveryTypes, setDeliveryTypes] = useState(null);
  const [provinceList, setProvinceList] = useState(null);
  const [agentList, setAgentList] = useState(null);
  const [orderStatus, setOrderStatus] = useState(null);

  const addressData = JSON.parse(localStorage.getItem('delivery-address')) || {
    province: '',
    city: '',
    suburb: '',
    address: '',
    postCode: '',
  };

  const [deliveryAddress, setDeliveryAddress] = useState(addressData);

  const initialState = {
    deliveryType,
    setDeliveryType,
    deliveryTypes,
    setDeliveryTypes,
    orderStatus,
    setOrderStatus,
    provinceList,
    setProvinceList,
    deliveryAddress,
    setDeliveryAddress(data) {
      localStorage.setItem('delivery-address', JSON.stringify(data));
      setDeliveryAddress(data);
    },
    setError,
    map,
    setMap,
    agentList,
    setAgentList,
    agentInfo,
    setAgentInfo,
    activated,
    setActivated,
  };

  function loadOrderStatus() {
    getOrderStatus()
      .then(order => setOrderStatus(order))
      .catch(e => setError(e.message));

    return () => setError(null);
  }

  useEffect(loadOrderStatus, []);

  if (!orderStatus) return null;

  return (
    <Router>
      <State.Provider value={initialState}>
        <StyledApp>
          <GlobalStyles />
          <AppHeader />
          {orderStatus.status && <Redirect to={URL.STATUS} />}
          <Switch>
            {routes.map(({ path, id, Component }) => (
              <Route key={id} path={path}>
                <React.Suspense fallback={null}>
                  <Component />
                </React.Suspense>
              </Route>
            ))}
          </Switch>
        </StyledApp>
        {apiError && (
          <div tw="flex items-center fixed bottom-0 p-16 text-16 bg-black-100 text-white w-full">
            <p>{apiError}</p>
            <button
              type="button"
              tw="ml-auto bg-gray-200 text-black-100 rounded-full p-4"
              onClick={() => setError(null)}
            >
              <CloseIcon />
            </button>
          </div>
        )}
      </State.Provider>
    </Router>
  );
}
