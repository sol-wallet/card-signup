export const URL = {
  HOME: '/',
  DELIVERY_METHOD: '/delivery-method',
  DELIVERY_ADDRESS: '/delivery-address',
  PICKUP: '/pickup',
  PAYMENT: '/payment',
  STATUS: '/status',
  CARD_ACTIVATION: '/activate',
};

export const DELIVERY_TYPE = {
  PICKUP: 'self_pickup_delivery',
  POST: 'post_delivery',
};

export const ORDER_STATUS = {
  NOT_PAID: 'not_paid',
  PAID: 'paid',
  IN_DELIVERY: 'in_delivery',
  ACTIVATED: 'activated',
};

export const PAYMENT_LINK = 'solwallet://link?action=open_screen&screen=top_up';
