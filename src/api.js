const BASE_URL = process.env.REACT_APP_BASE_URL;
const searchParams = new URLSearchParams(window.location.search);

const AUTH_TOKEN = searchParams.get('token') || '0282a868a01f50edada93483b3c30f0bb38028adae82c19f81e48c6484ecebfc';

async function request(url, { method = 'GET', ...params } = {}) {
  const resp = await fetch(`${BASE_URL}${url}`, {
    ...params,
    method,
    headers: {
      'X-Client-Type': 'android',
      'X-Client-Version': '181',
      'X-Access-Token': AUTH_TOKEN,
    },
  });

  if (!resp.ok) {
    throw new Error('There was an error contacting the server');
  }

  return resp.json();
}

export async function getDeliveryTypes() {
  const { result } = await request('/card/delivery-types');
  return (
    result?.delivery_types.map(({ delivery_type: type, title, amount, description }) => ({
      type,
      title,
      amount,
      description,
    })) || []
  );
}

export async function getOrderStatus() {
  const { result } = await request('/card/order-status');
  return result?.order_card || null;
}

export async function getProvinceList() {
  const { result } = await request('/general/province');
  return result?.province || [];
}

export async function getAgentList() {
  const { result } = await request('/card/order-agent-list');

  return result?.list.filter(({lat, lon}) => lat && lon).map(({lat, lon, ...agentData}) => {
    agentData.lat = +lat;
    agentData.lng = +lon;
    return agentData;
  });
}

export async function orderCard(params) {
  const { result } = await request('/card/order', { method: 'POST', body: JSON.stringify(params) });
  return result?.order_card;
}

export async function activateCard(params) {
  const { result } = await request('/card/activate', { method: 'POST', body: JSON.stringify(params) });
  return result;
}
