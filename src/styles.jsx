import tw, { styled } from 'twin.macro';

export const StyledApp = styled.div`
  ${tw`bg-black-400 text-white text-16 min-h-screen flex flex-col p-16 pt-0`}

  & h1 {
    ${tw`relative text-34 leading-56 tracking-tight font-bold z-10`}
  }
`;
