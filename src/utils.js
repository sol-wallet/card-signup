function parseTime(timeStr) {
  const [hours, minutes] = timeStr.split(':');
  const date = new Date();
  date.setHours(hours);
  date.setMinutes(minutes);
  return date;
}

export function isWorkingHours(workTimeStart, workTimeEnd) {
  const now = new Date();
  const workStart = parseTime(workTimeStart);
  const workEnd = parseTime(workTimeEnd);

  return now >= workStart && now <= workEnd;
}

export function isSameLocation(loc1, loc2) {
  if (!(loc1 && loc2)) return false;

  return loc1.lat === loc2.lat && loc1.lng === loc2.lng;
}

export function formatPhone(phoneStr) {
  return phoneStr.replace(/(\d{2})(\d{2})(\d{3})(\d{2})(\d+)/, '+$1 $2 $3 $4 $5');
}
