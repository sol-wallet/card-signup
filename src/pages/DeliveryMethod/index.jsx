import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import 'twin.macro';
import State from 'context';
import { URL, DELIVERY_TYPE } from 'const';
import { getDeliveryTypes } from 'api';

import deliveryImg from 'assets/img/delivery-method-img.png';
import { StyledImgContainer, StyledImg, StyledButton, StyledPersonIcon, StyledDeliveryIcon } from './styles';

export default function DeliveryMethod() {
  const { deliveryType, setDeliveryType, deliveryTypes, setDeliveryTypes, setError } = useContext(State);

  useEffect(() => {
    function fetchDeliveryTypes() {
      getDeliveryTypes()
        .then(data => setDeliveryTypes(data))
        .catch(e => setError(e.message));
    }

    if (!deliveryType) {
      fetchDeliveryTypes();
    }

    return () => setError(null);
  }, []);

  const history = useHistory();

  function selectDeliveryType(type) {
    setDeliveryType(type);

    if (type === DELIVERY_TYPE.PICKUP) {
      history.push(URL.PICKUP);
    } else if (type === DELIVERY_TYPE.POST) {
      history.push(URL.DELIVERY_ADDRESS);
    } else {
      throw new Error(`unknown delivery type: ${type}`);
    }
  }

  return (
    <>
      <StyledImgContainer>
        <StyledImg src={deliveryImg} tw="absolute" alt="Saly on a rocket thumbing up" />
      </StyledImgContainer>
      <div tw="pt-16">
        <h1 tw="mb-42">Choose delivery method</h1>
        {deliveryTypes &&
          deliveryTypes.map(({ type, title, description, amount }) => (
            <StyledButton key={title} tw="mb-16" onClick={() => selectDeliveryType(type)}>
              {type === DELIVERY_TYPE.PICKUP && <StyledPersonIcon />}
              {type === DELIVERY_TYPE.POST && <StyledDeliveryIcon />}
              <div tw="flex flex-col">
                <p tw="text-20 leading-24 font-bold mb-2">{title}</p>
                <p tw="text-gray-500 text-12 leading-18">
                  {description}&nbsp;&nbsp;<span tw="text-white font-medium">R {amount}</span>
                </p>
              </div>
            </StyledButton>
          ))}
      </div>
    </>
  );
}
