import tw, { styled } from 'twin.macro';

import { Button } from 'components/general';
import { PersonIcon, DeliveryIcon } from 'assets/icons';

export const StyledImgContainer = styled.div`
  ${tw`relative flex justify-center items-center pt-30`}
  margin: 0 -1.6rem;
  margin-top: -7rem;
  background: #8fc8ff;
  min-height: 26.9rem;
`;

export const StyledImg = styled.img`
  ${tw`absolute`};
  left: 1rem;
  top: 1.6rem;
`;

export const StyledButton = tw(
  Button,
)`text-white w-full relative flex items-center justify-center px-24 rounded-12 bg-black-200 active:bg-green-100`;

export const StyledPersonIcon = styled(PersonIcon)`
  ${tw`absolute top-0 bottom-0 m-auto`}
  left: 3.5rem;
`;
export const StyledDeliveryIcon = styled(DeliveryIcon)`
  ${tw`absolute top-0 bottom-0 m-auto`}
  left: 2.4rem;
`;
