import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import 'twin.macro';

import { orderCard } from 'api';

import { DELIVERY_TYPE, ORDER_STATUS, PAYMENT_LINK, URL } from 'const';
import State from 'context';
import { Button, FormItem } from 'components/general';
import { BikeDeliveryIcon, CashCounterIcon } from 'assets/icons';

export default function Payment() {
  const history = useHistory();
  const { deliveryType, deliveryTypes, deliveryAddress, agentInfo, setOrderStatus, setError } = useContext(State);

  useEffect(() => () => setError(null), []);

  if (!(deliveryType && deliveryTypes)) {
    history.replace(URL.DELIVERY_METHOD);
    return null;
  }

  async function handlePaymentConfirmation(payAgent = false) {
    const params = {
      delivery_type: deliveryType,
      address_line_1: '',
      suburb: '',
      town: '',
      province: '',
      post_code: '',
      pay_agent: payAgent,
    };

    if (deliveryType === DELIVERY_TYPE.PICKUP) {
      params.self_pickup_agent_phone = agentInfo.phone;
    } else {
      params.address_line_1 = deliveryAddress.address;
      params.suburb = deliveryAddress.suburb;
      params.town = deliveryAddress.city;
      params.province = deliveryAddress.province.value;
      params.post_code = deliveryAddress.postCode;
    }

    orderCard(params)
      .then(order => {
        setOrderStatus(order);
        localStorage.removeItem('delivery-address');

        if (order.status === ORDER_STATUS.NOT_PAID) {
          window.location.href = PAYMENT_LINK;
        } else {
          history.push(URL.STATUS);
        }
      })
      .catch(e => setError(e.message));
  }

  let BgImage;
  let actions;

  if (deliveryType === DELIVERY_TYPE.PICKUP) {
    BgImage = CashCounterIcon;
    actions = (
      <>
        <Button tw="mb-24" onClick={() => handlePaymentConfirmation(false)}>
          Agree
        </Button>
        <Button color="gray" onClick={() => handlePaymentConfirmation(true)}>
          I will pay the agent
        </Button>
      </>
    );
  } else {
    BgImage = BikeDeliveryIcon;
    actions = <Button onClick={() => handlePaymentConfirmation(false)}>Agree</Button>;
  }

  return (
    <>
      <BgImage tw="mb-16 mx-auto" />
      <h2 tw="text-34 leading-56 font-bold tracking-tight text-center mb-16">Pay activation fee</h2>
      <p tw="leading-20 tracking-01 mb-38">
        Значимость этих проблем настолько очевидна, что новая модель организационной деятельности требуют определения и
        уточнения
      </p>
      <FormItem
        id="amount-input"
        label="Amount"
        name="amount"
        tw="mb-36 text-white"
        value={deliveryTypes.find(dt => dt.type === deliveryType).amount}
        disabled
      />
      {actions}
    </>
  );
}
