import tw, { styled } from 'twin.macro';

export const StyledInfoCard = styled.div`
  ${tw`bg-black-200 flex flex-col rounded-10 shadow-sm p-20`}
`;
