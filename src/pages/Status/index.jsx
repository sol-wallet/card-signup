import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import tw from 'twin.macro';

import { Button } from 'components/general';
import Steps from 'components/Steps';
import dayjs from 'dayjs';

import State from 'context';
import { DELIVERY_TYPE, ORDER_STATUS, PAYMENT_LINK, URL } from 'const';
import { formatPhone } from 'utils';

import { StyledInfoCard } from './styles';

const statusMessages = {
  [ORDER_STATUS.NOT_PAID]: 'Waiting payment',
  [ORDER_STATUS.PAID]: 'Paid',
  [ORDER_STATUS.ACTIVATED]: 'Activated',
};

export default function Status() {
  const { orderStatus } = useContext(State);
  const history = useHistory();

  if (!orderStatus.status) {
    history.replace(URL.HOME);
    return null;
  }

  return (
    <>
      {orderStatus && orderStatus.delivery_type === DELIVERY_TYPE.PICKUP && (
        <StyledInfoCard tw="mb-36">
          <div tw="flex mb-20">
            <div tw="flex flex-col mr-22">
              <h2 tw="text-24 leading-28 font-medium mb-4">Order # {orderStatus.order_id}</h2>
              <p tw="text-14 leading-18 text-black-500">
                {dayjs(orderStatus.created_at).format('MMM DD, YYYY HH:mm A')}
              </p>
            </div>
            <p
              tw="text-12 leading-24 font-medium p-12 rounded-12 flex-grow text-center"
              css={[
                orderStatus.status === ORDER_STATUS.NOT_PAID && tw`bg-pink`,
                [ORDER_STATUS.PAID, ORDER_STATUS.IN_DELIVERY].includes(orderStatus.status) &&
                  tw`bg-blue-100 text-white`,
                orderStatus.status === ORDER_STATUS.ACTIVATED && tw`bg-green-400`,
              ]}
            >
              {statusMessages[orderStatus.status]}
            </p>
          </div>
          <p tw="text-black-600 mb-8">Your card is booked for you:</p>
          <p tw="font-medium tracking-02 mb-16">{orderStatus.self_pickup_agent.address}</p>
          <p tw="tracking-02 mb-16">
            <span tw="text-gray-700">Working hours:&nbsp;</span>
            {orderStatus.self_pickup_agent.work_time_begin} to {orderStatus.self_pickup_agent.work_time_end}
          </p>
          <p tw="tracking-02">
            <span tw="text-gray-700">Phone number:&nbsp;</span>
            {formatPhone(orderStatus.self_pickup_agent.phone)}
          </p>
        </StyledInfoCard>
      )}

      {orderStatus && orderStatus.delivery_type === DELIVERY_TYPE.POST && (
        <StyledInfoCard tw="text-center mb-36">
          <div tw="flex flex-col">
            <h2 tw="text-24 leading-28 font-medium mb-4">Order # {orderStatus.order_id}</h2>
            <p tw="text-14 leading-18 text-black-500">{dayjs(orderStatus.created_at).format('MMM DD, YYYY HH:mm A')}</p>
          </div>
          <Steps orderStatus={orderStatus.status} />
          <div tw="flex flex-col">
            <p tw="text-14 leading-18 text-gray-700 mb-12">Tracking number:</p>
            <p tw="text-20 leading-24 text-purple font-bold">{orderStatus.track_number || 'N/A'}</p>
          </div>
        </StyledInfoCard>
      )}

      {orderStatus.status === ORDER_STATUS.NOT_PAID && !orderStatus.pay_agent && (
        <Button tw="mb-24" href={PAYMENT_LINK}>
          Pay activation fee
        </Button>
      )}
      {orderStatus.status === ORDER_STATUS.IN_DELIVERY && (
        <Button to={URL.CARD_ACTIVATION} tw="mb-24">
          Activate card
        </Button>
      )}
      <Button href="solwallet://link?action=open_screen&screen=chat&message={text}" color="gray">
        I need help
      </Button>
    </>
  );
}
