import React, { useContext, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import 'twin.macro';

import State from 'context';

import { Button, FormItem } from 'components/general';
import { CardActivationIcon } from 'assets/icons';
import { activateCard } from 'api';
import { ORDER_STATUS, URL } from 'const';

export default function CardActivation() {
  const { setError, orderStatus, setOrderStatus } = useContext(State);
  const history = useHistory();
  const [activationNumber, setActivationNumber] = useState(null);

  useEffect(() => () => setError(null));

  function handleCardActivation() {
    activateCard({ card_voucher: activationNumber })
      .then(() => {
        setOrderStatus({ ...orderStatus, status: ORDER_STATUS.ACTIVATED });
        history.replace(URL.STATUS);
      })
      .catch(e => setError(e.message));
  }

  function handleInput(e) {
    setActivationNumber(e.target.value);
  }

  return (
    <>
      <CardActivationIcon tw="mb-16 mx-auto" />
      <h2 tw="text-34 leading-56 font-bold tracking-tight text-center mb-16">Activate the card</h2>
      <p tw="leading-20 tracking-01 mb-38">
        The activation code is hidden in your envelope, rip the envelope and write this code in the field
      </p>
      <FormItem
        id="code-input"
        label="Enter activation code"
        name="activationCode"
        mask="9999999"
        maskPlaceholder=""
        tw="mb-36"
        onChange={handleInput}
      />
      <Button onClick={handleCardActivation}>Activate</Button>
    </>
  );
}
