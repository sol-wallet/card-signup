export { default as Home } from './Home';
export { default as DeliveryMethod } from './DeliveryMethod';
export { default as DeliveryAddress } from './DeliveryAddress';
export { default as Pickup } from './Pickup';
export { default as Payment } from './Payment';
export { default as Status } from './Status';
export { default as CardActivation } from './CardActivation';
