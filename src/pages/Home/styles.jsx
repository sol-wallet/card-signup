import tw, { styled } from 'twin.macro';

export const StyledCardContainer = styled.div`
  ${tw`relative overflow-hidden flex justify-center items-center pt-30`}
  margin: 0 -1.6rem;
  margin-top: -7rem;
  background: linear-gradient(51.71deg, #000000 1.68%, #006887 89.25%);
  min-height: 26.9rem;
`;

export const StyledIcon = styled.div`
  ${tw`flex justify-center items-center w-64 h-64 bg-black-200 text-blue-200 rounded-full`}
`;
