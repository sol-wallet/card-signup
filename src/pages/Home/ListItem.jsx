import React from 'react';
import PropTypes from 'prop-types';
import 'twin.macro';

import { StyledIcon } from './styles';

export default function ListItem({ Icon, children }) {
  return (
    <li tw="flex items-center">
      <StyledIcon tw="mr-16">
        <Icon />
      </StyledIcon>
      <p tw="text-20">{children}</p>
    </li>
  );
}

ListItem.propTypes = {
  Icon: PropTypes.elementType.isRequired,
  children: PropTypes.node.isRequired,
};
