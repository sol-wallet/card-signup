import React from 'react';
import 'twin.macro';

import { CashIcon, PassportIcon, PercentIcon } from 'assets/icons';
import indexCard from 'assets/img/index-card.png';
import { Button } from 'components/general';

import ListItem from './ListItem';
import { StyledCardContainer } from './styles';

export default function HomePage() {
  return (
    <>
      <StyledCardContainer>
        <img src={indexCard} tw="absolute top-0" alt="sol card" />
      </StyledCardContainer>
      <div tw="pt-16">
        <h1 tw="mb-20">SOL CARD</h1>
        <ul tw="space-y-24 mb-24">
          <ListItem Icon={PercentIcon}>Free Swipes</ListItem>
          <ListItem Icon={CashIcon}>Withdraw in any ATM or Retail</ListItem>
          <ListItem Icon={PassportIcon}>Foreign passport allowed</ListItem>
        </ul>
        <a
          href="https://help.solcard.co.za/questions/how-to-start/how-to-get-sol-card"
          tw="inline-block text-blue-200 text-18 leading-none mb-20"
        >
          More details
        </a>
        <Button to="/delivery-method" tw="w-full">
          Apply now
        </Button>
      </div>
    </>
  );
}
