import React, { useState, useContext, useEffect } from 'react';
import 'twin.macro';

import { useHistory } from 'react-router-dom';
import { FormItem, Button } from 'components/general';
import { getProvinceList } from 'api';

import { URL } from 'const';
import State from 'context';

const rules = {
  required: ['province', 'city', 'address', 'postCode'],
};

export default function DeliveryPage() {
  const { deliveryAddress, setDeliveryAddress, provinceList, setProvinceList } = useContext(State);
  const history = useHistory();
  const [form, updateForm] = useState(deliveryAddress);

  async function loadProvinceList() {
    const list = await getProvinceList();
    setProvinceList(list.map(province => ({ value: province, label: province })));
  }

  useEffect(() => {
    if (provinceList) return;
    loadProvinceList();
  }, []);

  const hasErrors = rules.required.some(requiredField => !form[requiredField]) || form.postCode.length < 4;

  const handleChange = e => {
    updateForm(oldForm => ({ ...oldForm, [e.target.name]: e.target.value }));
  };

  const handleProvinceChange = option => {
    updateForm(oldForm => ({ ...oldForm, province: option }));
  };

  const submit = () => {
    setDeliveryAddress(form);
    history.push(URL.PAYMENT);
  };

  return (
    <form tw="flex flex-col space-y-26">
      <FormItem
        el="select"
        id="react-select-3-input"
        label="Province"
        name="province"
        isSearchable={false}
        value={form.province}
        options={provinceList}
        placeholder="Select province"
        onChange={handleProvinceChange}
      />
      <FormItem id="city-input" label="City" name="city" value={form.city} onChange={handleChange} />
      <FormItem id="suburb-input" label="Suburb" name="suburb" value={form.suburb} onChange={handleChange} />
      <FormItem
        id="address-input"
        label="Street and house"
        name="address"
        value={form.address}
        onChange={handleChange}
      />
      <FormItem
        id="post-code-input"
        type="tel"
        label="Post code"
        mask="9999"
        maskPlaceholder=""
        name="postCode"
        value={form.postCode}
        onChange={handleChange}
      />
      <Button onClick={submit} disabled={hasErrors}>
        Next
      </Button>
    </form>
  );
}
