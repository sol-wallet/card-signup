import React, { useContext, useState, useEffect } from 'react';
import 'twin.macro';
import State from 'context';

import Map from 'components/Map';
import { getAgentList } from 'api';

export default function Pickup() {
  const [selectedAgentIndex, setSelectedAgentIndex] = useState(null);
  const { agentList, setAgentList, setError } = useContext(State);

  useEffect(() => {
    function loadAgents() {
      getAgentList()
        .then(list => setAgentList(list))
        .catch(e => setError(e.message));
    }

    if (!agentList) {
      loadAgents();
    }

    return () => setError(null);
  }, []);

  return (
    <>
      {agentList && (
        <Map agents={agentList || []} selectedAgentIndex={selectedAgentIndex} changeSelected={setSelectedAgentIndex} />
      )}
      {!agentList && (
        <div tw="flex-grow flex flex-col justify-center text-24 leading-28 items-center">
          <p tw="mb-24">Loading</p>
        </div>
      )}
    </>
  );
}
